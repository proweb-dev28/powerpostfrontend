import React from 'react';
import Dropdown from 'react-toolbox/lib/dropdown';

const PPDropDown = (props) => <Dropdown {...props} />;

export default PPDropDown;
